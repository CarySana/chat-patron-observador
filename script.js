   // clase //
   class Subject {
    // constructor tendra el grupo de chismoso, el grupo es el arreglo//
    constructor() {
        this.observers = [];
    }

    // agregar nuevos sujetos al array de arriba //
    subscribe(o) {
        this.observers.push(o);
    }


    // notificar cuando cambie de estado a los chismoso "observers" //
    notify(model) {
        this.observers.forEach(observer => {
            observer.notify(model);
        })
    }

}

class TextSubject extends Subject {

    constructor() {
        super();  //llamar el constructor del padre //
        this.text = "";  //cadena//
    }

    notify(text) {
        this.text = text;  //el text reciba un texto//
        super.notify(this); //invocar el texto padre//
    }

}

class liOb1 {
    notify(subject) {
        document.getElementById("spanOb1").innerHTML =`<div class="bolita"><p>`+subject.text.length+`</p></div>`;
    }
}

class liOb2 {
    notify(subject) {
        document.getElementById("spanOb2").innerHTML =`<div class="bolita"><p>`+subject.text.length+`</p></div>`;
    }
}


var textSubject = new TextSubject();
let li1 = new liOb1();
textSubject.subscribe(li1);
let li2 = new liOb2();
textSubject.subscribe(li2);

document.getElementById("mnsj").addEventListener("input" ,(event) => {
    textSubject.notify(event.target.value);
        });